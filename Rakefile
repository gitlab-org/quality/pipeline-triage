# frozen_string_literal: true

require_relative 'scheduling'
require_relative 'dri_highlighter'
require_relative 'handover'

desc "Create and submit new pipeline triage schedule between today and no_of_days"
task :create_and_submit_schedule, [:no_of_days] do |t, args|
  args.with_defaults(:no_of_days => 90)
  schedule = Scheduling.new(args[:no_of_days])
  schedule.populate_schedule
  schedule.submit_schedule
end

desc "Create and print out to console new pipeline triage schedule between today and no_of_days"
task :create_schedule_only, [:no_of_days] do |t, args|
  args.with_defaults(:no_of_days => 90)
  Scheduling.new(args[:no_of_days]).populate_schedule
end

desc "Locate/highlight current week DRI in schedule then update README"
task :dri_highlighter do |t|
  DriHighlighter.new.run
end


desc "Create new triage issue and close the previous one"
task :hand_over_triage_issue do |t|
  handover = Handover.new
  handover.open_new_triage_issue
  handover.close_past_triage_report
end

desc "Only generate the new triage schedules, not creating nor closing issue"
task :view_new_triage_schedule_locally do |t|
  handover = Handover.new
  handover.compile_new_triage_report_info
end