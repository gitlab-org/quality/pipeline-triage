# frozen_string_literal: true

require './client'
require 'json'
require 'uri'
require 'markdown-tables'
require 'yaml'

# This script is to a new issue for pipeline triage and close out the previous issue.
#
# Commands:
# rake hand_over_triage_issue
# rake view_new_triage_schedule_locally

class Handover
  def initialize
    @future_schedule = future_schedule
    @name_list = YAML.load_file('SETs_name_list.yml')
    @client = Client.new
    @previous_issue_iid = get_latest_triage_issue_iid
  end

  def compile_new_triage_report_info
    puts "🤖: The next 2 weeks expected DRIs are:\n#{timezone_dris_table}}"
    puts "🤖: There are no carryovers from last week..." if carryovers.empty?
    puts "🤖: There are #{carryovers.size} carryovers from last week - issue id #{@previous_issue_iid}"
  end

  def open_new_triage_issue
    compile_new_triage_report_info
    issue = @client.open_triage_issue(issue_title, template, assignee_ids, labels: 'triage report')
    puts "🤖: Hooray, new triage report opened at https://gitlab.com/gitlab-org/quality/pipeline-triage/-/issues/#{issue.iid}"
  end

  def close_past_triage_report
    @client.close_past_triage_report(@previous_issue_iid)
    puts "🤖: Closed last week triage issue id #{@previous_issue_iid}. Bye 👋"
  end

  private

  def get_latest_triage_issue_iid
    @client.get_latest_triage_issue.iid
  end

  def issue_title
    "Pipeline Triage Report from #{start_date} to #{end_date}"
  end

  def future_schedule
    schedule = YAML.load_file('new_dri_schedule.yml', permitted_classes: [Date])

    # Date.wday goes from [0 - 6] (from Sunday - Saturday).
    # If today.between?(1, 5) - i.e. today is from monday to friday - get this week's Monday
    # by setting the date back to 1 week. Then reject all rows with date that is <= this date
    #
    # E.g: If today is Monday 2024-05-27, date should be 2024-05-20, reject all rows with date <= 2024-05-20
    today = Date.today
    date = (today.wday.between?(1, 5)) ? (today - 7) : today

    schedule.reject! do |row|
      Date.parse(row[0].to_s) <= date
    end

    schedule
  end

  def start_date
    @future_schedule.first[0]
  end

  def end_date
    converted_start_date = Date::strptime(start_date.to_s, "%Y-%m-%d")
    (converted_start_date + 4).to_s
  end

  def next_start_date
    @future_schedule[1][0]
  end

  def apac_dri
    @future_schedule.first[1]
  end

  def emea_dri
    @future_schedule.first[2]
  end

  def amer_dri
    @future_schedule.first[3]
  end

  def next_apac_dri
    @future_schedule[1][1]
  end

  def next_emea_dri
    @future_schedule[1][2]
  end

  def next_amer_dri
    @future_schedule[1][3]
  end

  def current_dris
    current_dris = {}

    @name_list.each do |person|
      name = person['name']
      handle = person['handle']
      if apac_dri == name
        current_dris[:apac] = { name: name, handle: handle }
      elsif emea_dri == name
        current_dris[:emea] = { name: name, handle: handle }
      elsif amer_dri == name
        current_dris[:amer] = { name: name, handle: handle }
      end
    end

    current_dris
  end

  def next_dris
    next_dris = {}

    @name_list.each do |person|
      name = person['name']
      handle = person['handle']
      if next_apac_dri == name
        next_dris[:apac] = { name: name, handle: handle }
      elsif next_emea_dri == name
        next_dris[:emea] = { name: name, handle: handle }
      elsif next_amer_dri == name
        next_dris[:amer] = { name: name, handle: handle }
      end
    end

    next_dris
  end

  def assignee_ids
    assignee_ids = []

    current_dris.each_value do |value|
      user = @client.get_user_id_by_handle(value[:handle])
      assignee_ids << user.first&.fetch("id")
    end

    assignee_ids
  end

  def timezone_dris_table
    labels = ['Week of', 'APAC', 'EMEA', 'AMER']
    data = [
      [
        start_date,
        "#{current_dris[:apac][:name]} (@#{current_dris[:apac][:handle]})",
        "#{current_dris[:emea][:name]} (@#{current_dris[:emea][:handle]})",
        "#{current_dris[:amer][:name]} (@#{current_dris[:amer][:handle]})"
      ],
      [
        next_start_date,
        "#{next_dris[:apac][:name]} (@#{next_dris[:apac][:handle]})",
        "#{next_dris[:emea][:name]} (@#{next_dris[:emea][:handle]})",
        "#{next_dris[:amer][:name]} (@#{next_dris[:amer][:handle]})"
      ]
    ]

    # Align left
    MarkdownTables.make_table(labels, data, is_rows: true, align: %w[l l l l])
  end

  def all_notes_from_handover_report
    @client.get_notes_from_issue(@previous_issue_iid)
  end

  def carryovers
    issues = []
    carryovers = []
    format = "https://gitlab.com/gitlab-org/gitlab/-/issues/"

    all_notes_from_handover_report.each do |note|
      if note.body.include?("|:-|:-|:-|:-|:-|")
        links = URI.extract(note.body, %w[https]).to_a
        links.each do |link|
          if link.include?(format) && !link.include?("#stack-trace") && !issues.include?(link)
            issues << link
          end
        end
      end
    end

    issues.each do |issue|
      iid = issue.scan(/\d/).join('')
      if @client.issue_opened?(iid)
        carryovers << "* #{issue}+"
      end
    end

    carryovers
  end

  def template
    <<~EOF
      # DRI

      Please review the
      [responsibilities](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/oncall-rotation/#responsibility)
      and [guidelines](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/debugging-end-to-end-test-failures/)
      if you have not done so recently.

      Above all else, please remember that the aim of pipeline triage is to identify problems and try to
      get them **resolved** (ideally) _before_ they impact users.

      ## Resources

      - [Debugging Failing E2E Tests and Test Pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/debugging-end-to-end-test-failures/)
      - The [dri gem](https://gitlab.com/gitlab-org/ruby/gems/dri) (to help report and triage issues)
      - [DX Runbooks](https://gitlab.com/gitlab-org/quality/runbooks) (feel free to add more)
      - [Test execution dashboards](https://dashboards.quality.gitlab.net/)
      - [Recently updated failure issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&or%5Blabel_name%5D%5B%5D=failure%3A%3Abroken-test&or%5Blabel_name%5D%5B%5D=failure%3A%3Abug&or%5Blabel_name%5D%5B%5D=failure%3A%3Anew&or%5Blabel_name%5D%5B%5D=failure%3A%3Ainvestigating&or%5Blabel_name%5D%5B%5D=failure%3A%3Atest-environment&or%5Blabel_name%5D%5B%5D=failure%3A%3Aflaky-test&or%5Blabel_name%5D%5B%5D=failure%3A%3Astale-test&first_page_size=100)
      - [Non-quarantined test results in the testcases project](https://gitlab.com/gitlab-org/quality/testcases/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=status%3A%3Aautomated&not%5Blabel_name%5D%5B%5D=quarantine&first_page_size=100)

      #### DRI Schedule

      #{timezone_dris_table}

      # Carryovers

      #{carryovers.join("\n")}

      # [Current known issues for CustomersDot project](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Quality%3Ae2e-test&label_name[]=staging%3A%3Afailed)

      /label ~Quality ~"triage report"

      /assign
    EOF
  end
end
