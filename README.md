## DRI triage issue handover
`alfred-the-butler` 🤖 creates and assigns the triage issue to corresponding DRIs and pings the next week's DRIs 
as well, according to the rotation schedule below. The issue is 
[scheduled](https://gitlab.com/gitlab-org/quality/pipeline-triage/-/pipeline_schedules) 
to open on Saturdays prior upcoming workweek.

It is also carrying over the issues from last week to contextualise the current DRI of past failures. 
The carryovers are issues that remained opened since reported on the previous week. 
These are automatically generated and included in the Pipeline Triage Issue.

When it's time to rotate project token, simply navigate to Project -> Settings -> Access Tokens then 
`Add new token` with scopes `api, read_api, read_repository, write_repository`. You can keep the same name 
or name it something else, and delete the old token. After that, navigate to Project -> Settings -> CI/CD and 
update variable `ACCESS_TOKEN` with the new token's value.

## DRI Gem
Looking for some help triaging failures this week? Take a look at the 
[dri gem](https://gitlab.com/gitlab-org/quality/dri) to help reporting to this project your triaging progress.

## DRI weekly [rotation] schedule

|**Start Date**|**APAC [DRI]**|**EMEA [DRI]**|**AMER [DRI]**|
|:-|:-|:-|:-|
|2024-12-30|Harsha Muralidhar|Ievgen Chernikov|Dan Davison|
|2025-01-06|N/A(1)|Nailia Iskhakova|Valerie Burton|
|2025-01-13|Vishal Patel|John McDonnell|Andy Hohenner|
|2025-01-20|N/A(2)|Sofia Vistas|Tiffany Rea|
|2025-01-27|Sanad Liaquat|N/A(1)|Chloe Liu|
|2025-02-03|Jay McCure|Will Meek|Richard Chong|
|2025-02-10|N/A(3)|Andrejs Cunskis|Désirée Chevalier|
|2025-02-17|Harsha Muralidhar|N/A(2)|Brittany Wilkerson|
|2025-02-24|N/A(1)|Nivetha Prabakaran|Jim Baumgardner|
|2025-03-03|Vishal Patel|Ievgen Chernikov|Joy Roodnick|
|:point_right: **2025-03-10**|**N/A(2)**|**N/A(3)**|**Dan Davison**|
|2025-03-17|Harsha Muralidhar|John McDonnell|N/A(4)|
|2025-03-24|Jay McCure|Sofia Vistas|Andy Hohenner|
|2025-03-31|N/A(3)|N/A(1)|Tiffany Rea|
|2025-04-07|Sanad Liaquat|Will Meek|Chloe Liu|
|2025-04-14|N/A(1)|Andrejs Cunskis|Richard Chong|
|2025-04-21|Vishal Patel|N/A(2)|Désirée Chevalier|
|2025-04-28|N/A(2)|Nivetha Prabakaran|Brittany Wilkerson|

[DRI]: https://handbook.gitlab.com/handbook/people-group/directly-responsible-individuals/
[rotation]: https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/

_Any changes made to this `README.md` will get overwritten weekly by DRI highlighter task._
