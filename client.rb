require 'gitlab'

class Client
  TRIAGE_PROJECT_ID = '15291320'
  API_URL = 'https://gitlab.com/api/v4'
  TOKEN = ENV['ACCESS_TOKEN']
  GITLAB_PROJECT_ID = '278964'

  def initialize
    message = "\e[31m[ERROR] ACCESS_TOKEN (your PAT) is missing. Please provide and retry.\e[0m"
    abort(message) if TOKEN.nil?
    gitlab
  end

  def get_user_id_by_handle(handle)
    gitlab.users(
      username: handle
    )
  end

  def get_latest_triage_issue
    issues = gitlab.issues(
      TRIAGE_PROJECT_ID,
      state: 'opened',
      order_by: 'created_at',
      scope: 'all'
    )

    issues.find do |issue|
      issue.title.include?('Pipeline Triage Report')
    end
  end

  def get_notes_from_issue(iid)
    gitlab.issue_notes(
      TRIAGE_PROJECT_ID,
      iid
    )
  end

  def issue_opened?(iid)
    issue = gitlab.issues(
      GITLAB_PROJECT_ID,
      state: 'opened',
      scope: 'all',
      iids: iid
    )
    !issue.empty?
  end

  def open_triage_issue(title, template, assignees, labels: nil)
    gitlab.create_issue(
      TRIAGE_PROJECT_ID,
      title,
      description: template,
      assignee_ids: assignees,
      labels: labels
    )
  end

  def close_past_triage_report(iid)
    gitlab.close_issue(
      TRIAGE_PROJECT_ID,
      iid
    )
  end

  def create_branch(branch, ref)
    puts "Creating new branch - #{branch}..."

    gitlab.create_branch(TRIAGE_PROJECT_ID, branch, ref)
  end

  def create_commit(branch, message, actions)
    puts "Committing changes to branch #{branch}..."

    gitlab.create_commit(TRIAGE_PROJECT_ID, branch, message, actions)
  end

  def create_merge_request(mr_description, title, source_branch)
    puts "Creating merge request from branch - #{source_branch}..."

    options = {
      description: mr_description,
      source_branch: source_branch,
      target_branch: 'master',
      remove_source_branch: true,
      squash: true,
      labels: 'Quality'
    }

    mr = gitlab.create_merge_request(TRIAGE_PROJECT_ID, title, options)

    mr.to_hash
  end

  def accept_merge_request(mr_iid)
    ensure_mr_can_be_merged(mr_iid)

    puts "Merging MR with ID #{mr_iid}..."
    gitlab.accept_merge_request(TRIAGE_PROJECT_ID, mr_iid)
  end

  private

  def gitlab
    @gitlab ||= Gitlab.client(
      endpoint: API_URL,
      private_token: TOKEN
    )
  end

  def ensure_mr_can_be_merged(mr_iid)
    puts "Waiting for MR with ID #{mr_iid} to become ready for merging..."

    60.times do |i|
      status = merge_request_status(mr_iid)
      if status == 'mergeable'
        break
      else
        if i < 59
          sleep 1
        else
          message = "\e[31m[ERROR] MR ID #{mr_iid} is still not mergeable after 1 minute - Current MR status #{status}.\e[0m"
          abort(message)
        end
      end
    end
  end

  def merge_request_status(mr_iid)
    gitlab.merge_request(TRIAGE_PROJECT_ID, mr_iid).detailed_merge_status
  end
end
